# checklen extension for zx2c4's pass

Report passwords which length is below a certain value.

# Install

Copy `checklen.bash` into the password-store/extension folder

```bash
PASS_EXT="${PASSWORD_STORE_DIR:-$HOME/.password-store}/.extensions/"
mkdir -p "$PASS_EXT"
cp checklen.bash "$PASS_EXT"
```

Also make sure the `PASSWORD_STORE_ENABLE_EXTENSIONS` env variable is set to `true`

# Usage

```bash
# List passwords below 8 characters
pass checklen 8
```
