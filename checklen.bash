#!/bin/bash

if [[ "$1" == "" || "$1" == "--help" ]]; then
	echo "Usage: pass checklen [min-length]"
	exit 0
fi


set -euo pipefail

check(){
	FPATH="$(realpath --relative-to "$PREFIX" "$1")"
	PASSPATH="$(dirname "$FPATH")/$(basename "$FPATH" ".gpg")"
	LEN=$(( "$(pass show "$PASSPATH" | head -n1 | wc -m)" - 1 ))
	if (( LEN <= $2 )); then
		echo "$1: $LEN"
	fi
}
export PREFIX
MINLEN=${1:-8}
find "$PREFIX" -name "*.gpg" | while read -r F; do check "$F" "$MINLEN"; done

